# ImageSaveTest

This is a simple Xamarin.Forms with Prism app that demonstrates an issue with System.IO.File.Copy.

It uses Xam.Plugin.Media to import an image and copy it to a new directory (/storage/emulated/0/Pictures/com.companyname.imagesavetest).

Then, on a press of a different button, the image is copied to /storage/emulated/0/. This directory is scanned by apps such as Snapchat and Signal when searching for pictures on the phone. However these apps do not detect the picture.

If the image is renamed (eg to a new name, then back to the original name) or copied/moved to a new directory and then back to the original directory by either Windows or by a file manager on the phone, these apps will then detect it.

According to an MD5 hash and to the "stat" tool, the files before and after the renaming are identical, but are only detected after the rename.