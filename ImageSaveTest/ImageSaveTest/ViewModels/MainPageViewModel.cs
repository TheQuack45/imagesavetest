﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace ImageSaveTest.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        public string _imageName;
        private string _imagePath;

        private Command _addImageCommand;
        public Command AddImageCommand
        {
            get
            {
                if (this._addImageCommand == null)
                    { this._addImageCommand = new Command(this.AddImage); }
                return this._addImageCommand;
            }
        }

        private Command _saveImageCommand;
        public Command SaveImageCommand
        {
            get
            {
                if (this._saveImageCommand == null)
                    { this._saveImageCommand = new Command(this.SaveImage); }
                return this._saveImageCommand;
            }
        }

        public MainPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "Main Page";
        }

        private async void AddImage()
        {
            await CrossPermissions.Current.RequestPermissionsAsync(new[] { Plugin.Permissions.Abstractions.Permission.Storage });

            MediaFile selectedPicture = await CrossMedia.Current.PickPhotoAsync();
            if (selectedPicture == null)
            {
                // No picture was selected. Do nothing.
                return;
            }

            string extension = MimeManager.ExtensionFromPath(selectedPicture.Path);
            System.IO.Directory.CreateDirectory(Dirs.Images);
            this._imageName = $"testpic.{extension}";
            this._imagePath = System.IO.Path.Combine(Dirs.Images, this._imageName);
            System.IO.File.Copy(selectedPicture.Path, this._imagePath, true);
        }

        private void SaveImage()
        {
            string newPath = System.IO.Path.Combine("/storage/emulated/0", this._imageName);
            System.IO.File.Copy(this._imagePath, newPath);
            DateTime now = DateTime.Now;
            System.IO.File.SetLastWriteTime(newPath, now);
            System.IO.File.SetLastAccessTime(newPath, now);
        }
    }
}
